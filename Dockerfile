FROM golang:latest

MAINTAINER rENYI "renyinew@gmail.com"

ADD . $GOPATH/src/app
RUN go get app
RUN CGO_ENABLED=0 go install -a app

EXPOSE 80
CMD app
